﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public GameObject Bullet;
	public Transform innerCamera;
	void Update () {
		if (Input.GetKeyUp(KeyCode.Space) || Input.GetMouseButtonDown(0))
		{
			Shoot ();
		}
	}

	private void Shoot()
	{
		var go = Instantiate<GameObject> (Bullet);
		go.transform.position = innerCamera.position;
		var bullet = go.GetComponent<Bullet> ();
		bullet.SetDirection (innerCamera.forward);
	}
}
