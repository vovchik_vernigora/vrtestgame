﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ZombiSpawner : MonoBehaviour {

	public List<Transform> spawnPoints;
	public float spawnTime = 3;
	public PlayerController player;
	public GameObject zombiPrefab;

	void Start () 
	{
		StartCoroutine (SpawnZombi (spawnTime, zombiPrefab));	
	}
	
	private IEnumerator SpawnZombi(float delay, GameObject zombi)
	{
		while (true) 
		{
			yield return new WaitForSeconds (delay);
			var go = Instantiate<GameObject> (zombi);
			go.transform.position = spawnPoints [Random.Range (0, spawnPoints.Count)].position;
			go.GetComponent<Zombi> ().Init (player.transform);
		}
	}
}
