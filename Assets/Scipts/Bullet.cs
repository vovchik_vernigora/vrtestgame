﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	public float Speed = 10;
	public float LifeTime = 1;

	private Vector3 direction = Vector3.zero;

	void Start () 
	{
		StartCoroutine (SelfDestroyer (LifeTime));
	}

	public void SetDirection(Vector3 direction)
	{
		this.direction = direction;
	}

	void Update()
	{
		if (direction == Vector3.zero) 
		{
			return;
		}

		transform.position += direction * Speed * Time.deltaTime;
	}

	private IEnumerator SelfDestroyer(float timeToDestroy)
	{
		yield return new WaitForSeconds (timeToDestroy);

		Destroy (gameObject);
	}
}
