﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Zombi : MonoBehaviour {

	public List<AudioClip> listOfSounds;
	public AudioSource audioplayer;
	public Vector2 speed;
	private float timeToGetToPlayer;
	private Transform playerTr = null;
	private float moveTime = 0;
	private Vector3 startPosition;

	public void Init (Transform playerTr) 
	{
		this.playerTr = playerTr;
		transform.LookAt (playerTr.transform);
		timeToGetToPlayer = Vector3.Distance (transform.position, playerTr.position) / Random.Range (speed.x, speed.y) * Time.deltaTime;
		startPosition = transform.position;

		StartCoroutine (MoveToPlayer ());
		StartCoroutine (SoundPlayer ());
	}

	IEnumerator MoveToPlayer () 
	{
		yield return new WaitForSeconds (2);

		while (true) 
		{
			moveTime += Time.deltaTime;
			transform.position = Vector3.Lerp(startPosition, playerTr.position, moveTime / timeToGetToPlayer);

			if (moveTime >= timeToGetToPlayer) 
			{
				Debug.Log ("you been eated");
			}
			yield return null;
		}
	}

	IEnumerator SoundPlayer ()
	{
		while (true) 
		{
			yield return new WaitForSeconds (2);
			audioplayer.clip = listOfSounds [Random.Range (0, listOfSounds.Count)];
			audioplayer.Play ();
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.layer == LayerMask.NameToLayer ("Bullet")) 
		{
			Destroy (gameObject);
			Destroy (other.gameObject);
		}
	}
}
